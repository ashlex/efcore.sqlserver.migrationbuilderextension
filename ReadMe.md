# EFCore.SqlServer.MigrationBuilderExtensions


[![Nuget (with prereleases)](https://img.shields.io/nuget/vpre/EFCore.SqlServer.MigrationBuilderExtensions)](https://www.nuget.org/packages/EFCore.SqlServer.MigrationBuilderExtensions)

This tools provide extended builders to create migration.

## Examples

### Create and drop view example    
-----

```csharp
public class InitialMigration : Migration
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateView(builder => Build__custom_view_name__View(builder));
    }
    
    protected override void Down(MigrationBuilder migrationBuilder)
    {
         migrationBuilder.DropView(builder => Build__custom_view_name__View(builder));
    }
    
    internal static ViewBuilder Build__custom_view_name__View(ViewBuilder viewBuilder)
    {
        return viewBuilder.Scheme("scheme_name").Name("view_name").Body(Body__custom_view_name__View());
    }
    
    private static string Body__custom_view_name__View()
    {
        return "SELECT ...";
    }
}
```


### Alter view example
-----

```csharp
public class OtherMigration : Migration
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.AlterView(builder => Build__custom_view_name__View(builder));
    }
    
    protected override void Down(MigrationBuilder migrationBuilder)
    {
         migrationBuilder.AlterView(builder => InitialMigration.Build__custom_view_name__View(builder));
    }
    
    internal static ViewBuilder Build__custom_view_name__View(ViewBuilder viewBuilder)
    {
        return InitialMigration.Build__custom_view_name__View(viewBuilder)
            .AddDescription(d=>d
                        .AddChangeLog(log=>log
                            .Author("Name")
                            .Summary("Example summary")
                            .CreationDate("20.05.2019")))
            .Body(Body__custom_view_name__View());
    }
    
    private static string Body__custom_view_name__View()
    {
        return "SELECT ...";
    }
}
```

### Create and drop trigger example
-----

```csharp
public class InitialMigration : Migration
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTrigger(builder => Build__custom_trigger_name_insert_update__Trigger(builder));
    }
    
    protected override void Down(MigrationBuilder migrationBuilder)
    {
         migrationBuilder.DropTrigger(builder => Build__custom_trigger_name_insert_update__Trigger(builder));
    }
    
    internal static TriggerBuilder Build__custom_trigger_name_insert_update__Trigger(TriggerBuilder triggerBuilder)
    {
        return triggerBuilder.
            .TargetName("trigger_target")
            .TargetScheme("target_scheme")
            .Name("trigger_name")
            .Scheme("trigger_scheme")
            .AddAfterAction(TriggerAction.INSERT)
            .AddAfterAction(TriggerAction.UPDATE)
            .Body(Body__custom_trigger_name_insert_update__Trigger());
    }
    
    private static string Body__custom_trigger_name_insert_update__Trigger()
    {
        return "...";
    }
}
```

### Create and drop stored procedure example   
-----

```csharp
public class InitialMigration : Migration
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateStoredProcedure(builder => Build__custom_stored_procedure_name__StoredProcedure(builder));
    }
    
    protected override void Down(MigrationBuilder migrationBuilder)
    {
         migrationBuilder.DropStoredProcedure(builder => Build__custom_stored_procedure_name__StoredProcedure(builder));
    }
    
    internal static TriggerStoredProcedure Build__custom_stored_procedure_name__StoredProcedure(StoredProcedureBuilder storedProcedureBuilder)
    {
        return storedProcedureBuilder.
            .Name("sp_name")
            .Scheme("sp_scheme")
            .AddDescription(b => b
                .Author("Name")
                .Summary("Anything comment"))
            .AddInputParameter(pb => pb.Name("p1").Type("varchar(31)").Order(1))
            .AddInputParameter(pb => pb.Name("p2").Type("varchar(31)").Order(2))
            .Body(Body__custom_stored_procedure_name__StoredProcedure());
    }
    
    private static string Body__custom_stored_procedure_name__StoredProcedure()
    {
        return "<Sql stored procedure body>";
    }
}
```