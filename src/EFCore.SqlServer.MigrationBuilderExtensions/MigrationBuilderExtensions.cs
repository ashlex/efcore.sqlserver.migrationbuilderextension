using System;
using System.Globalization;
using System.Reflection;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;
using EFCore.SqlServer.MigrationBuilderExtensions.StoredProcedure;
using EFCore.SqlServer.MigrationBuilderExtensions.Triggers;
using EFCore.SqlServer.MigrationBuilderExtensions.Views;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCore.SqlServer.MigrationBuilderExtensions
{
    public static class MigrationBuilderExtensions
    {

        /// <summary>
        /// Create a trigger
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="triggerBuilder"><see cref="TriggerBuilder"/></param>
        /// <returns></returns>
        public static MigrationBuilder CreateTrigger(this MigrationBuilder builder, Action<TriggerBuilder> triggerBuilder)
        {
            return builder.Execute<TriggerElement,TriggerBuilder>(triggerBuilder,BuilderAction.CREATE);
        }

        /// <summary>
        /// Changes the properties of an existing trigger
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="triggerBuilder"><see cref="TriggerBuilder"/></param>
        /// <returns></returns>
        public static MigrationBuilder AlterTrigger(this MigrationBuilder builder, Action<TriggerBuilder> triggerBuilder)
        {
            return builder.Execute<TriggerElement,TriggerBuilder>(triggerBuilder,BuilderAction.ALTER);
        }

        /// <summary>
        /// Removes trigger
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="scheme"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MigrationBuilder DropTrigger(this MigrationBuilder builder, string scheme, string name)
        {
            builder.Sql("DROP TRIGGER [" + scheme + "].[" + name + "]");
            return builder;
        }
        
        public static MigrationBuilder DropTrigger(this MigrationBuilder builder, Action<TriggerBuilder> triggerBuilder)
        {
            
            return builder.Execute<TriggerElement,TriggerBuilder>(triggerBuilder,BuilderAction.DROP);
        }

        /// <summary>
        /// Creating stored procedures
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="spBuilder"></param>
        /// <returns></returns>
        public static MigrationBuilder CreateStoredProcedure(this MigrationBuilder builder,
            Action<StoredProcedureBuilder> spBuilder)
        {
            return builder.Execute<StoredProcedureElement,StoredProcedureBuilder>(spBuilder,BuilderAction.CREATE);
        }

        /// <summary>
        /// Change stored procedure
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="spBuilder"></param>
        /// <returns></returns>
        public static MigrationBuilder AlterStoredProcedure(this MigrationBuilder builder,
            Action<StoredProcedureBuilder> spBuilder)
        {
            return builder.Execute<StoredProcedureElement,StoredProcedureBuilder>(spBuilder,BuilderAction.ALTER);
        }

        /// <summary>
        /// Deleting a stored procedure
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="scheme"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MigrationBuilder DropStoredProcedure(this MigrationBuilder builder, string scheme,string name)
        {
            builder.Sql("DROP PROCEDURE [" + scheme + "].[" + name + "]");
            return builder;
        }
        public static MigrationBuilder DropStoredProcedure(this MigrationBuilder builder, Action<StoredProcedureBuilder> spBuilder)
        {
            return builder.Execute<StoredProcedureElement,StoredProcedureBuilder>(spBuilder,BuilderAction.DROP);
        }

        /// <summary>
        /// Creating a view
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="viewBuilder"></param>
        /// <returns></returns>
        public static MigrationBuilder CreateView(this MigrationBuilder builder,
            Action<ViewBuilder> viewBuilder)
        {
            return builder.Execute<ViewElement,ViewBuilder>(viewBuilder,BuilderAction.CREATE);
        }

        /// <summary>
        /// Modifies a previously created view
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="viewBuilder"></param>
        /// <returns></returns>
        public static MigrationBuilder AlterView(this MigrationBuilder builder,
            Action<ViewBuilder> viewBuilder)
        {
            return builder.Execute<ViewElement,ViewBuilder>(viewBuilder,BuilderAction.ALTER);
        }

        /// <summary>
        /// Removes existing view
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="scheme"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static MigrationBuilder DropView(this MigrationBuilder builder, string scheme,string name)
        {
            builder.Sql("DROP VIEW [" + scheme + "].[" + name + "]");
            return builder;
        }
        public static MigrationBuilder DropView(this MigrationBuilder builder, 
            Action<ViewBuilder> viewBuilder)
        {
            return builder.Execute<ViewElement,ViewBuilder>(viewBuilder,BuilderAction.DROP);
        }
        
        
        /// <summary>
        /// Switch check constraint
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="scheme"></param>
        /// <param name="name"></param>
        /// <param name="table"></param>
        /// <param name="isEnable"></param>
        /// <returns></returns>
        public static MigrationBuilder CheckConstraint(this MigrationBuilder builder, string scheme,string name,string table,bool isEnable)
        {
            builder.Sql($"ALTER TABLE [{scheme}].[{table}] {(isEnable?"CHECK":"NOCHECK")} CONSTRAINT [{name}]");
            return builder;
        }
        
        
        private static MigrationBuilder Execute<T,T1>(this MigrationBuilder builder,Action<T1> act, BuilderAction buildAction) where T:ElementBase, new() where T1:ElementBuilder<T,T1>
        {
//            System.Diagnostics.Debugger.Launch();
            T el=new T();
            el.BuilderAction = buildAction;
            Type type = typeof(T1);
            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;
            T1 t=(T1)Activator.CreateInstance(type, flags,null,new object[]{el},(CultureInfo)null);
            act(t);
            builder.Sql(el.ToSQL());
            return builder;
        }
    }
}