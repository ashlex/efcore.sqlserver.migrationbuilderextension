using System;
using System.Collections.Generic;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;

namespace EFCore.SqlServer.MigrationBuilderExtensions.StoredProcedure
{
    public class StoredProcedureBuilder:ElementBuilder<StoredProcedureElement,StoredProcedureBuilder>
    {
        internal StoredProcedureBuilder(StoredProcedureElement storedProcedure) : base(storedProcedure)
        {
        }

        public StoredProcedureBuilder AddInputParameter(Action<InputParameterBuilder> parameterBuilder)
        {
            InputParameter inputParameter=new InputParameter();
            parameterBuilder(new InputParameterBuilder(inputParameter));
            if(_target.InputParameters==null)_target.InputParameters=new List<InputParameter>();
            _target.InputParameters.Add(inputParameter);
            return this;
        }
        
    }
}