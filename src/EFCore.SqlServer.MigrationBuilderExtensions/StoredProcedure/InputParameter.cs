namespace EFCore.SqlServer.MigrationBuilderExtensions.StoredProcedure
{
    public class InputParameter
    {
        internal string Name { get; set; }
        internal string Type { get; set; }
        internal string DefaultValue { get; set; }
        internal int Order { get; set; }

        internal string ToSql()
        {
            return $"@{Name}   {Type} {(DefaultValue!=null ? $"= {DefaultValue}" : "")}";
        }
    }
}