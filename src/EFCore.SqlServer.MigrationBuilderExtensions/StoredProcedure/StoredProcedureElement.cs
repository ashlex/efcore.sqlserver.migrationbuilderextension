using System.Collections.Generic;
using System.Text;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;

namespace EFCore.SqlServer.MigrationBuilderExtensions.StoredProcedure
{
    public class StoredProcedureElement:ElementBase
    {
        internal List<InputParameter> InputParameters { get; set; }
        
        private string Parameters
        {
            get
            {
                StringBuilder sb=new StringBuilder();
                InputParameters.Sort((p1, p2) => p1.Order.CompareTo(p2.Order));
                foreach (InputParameter parameter in InputParameters)
                {
                    sb.Append(parameter.ToSql()).AppendLine(", ");
                }

                return sb.ToString().TrimEnd(' ', ',','\r','\n');
            }
        }

        internal override string ToSQL()
        {
            if (BuilderAction == BuilderAction.DROP) 
                return $"DROP {CurrentElementType} {FullName}";
            
            StringBuilder sb=new StringBuilder(Description?.ToSql());
            sb.Append(BuilderAction).Append(" ").Append(CurrentElementType).Append(" ").AppendLine(FullName);
            if (InputParameters != null && InputParameters.Count > 0)
                sb.AppendLine(Parameters);
            sb.AppendLine("AS")
                .AppendLine("BEGIN")
                .AppendLine(Body)
                .AppendLine("END");
            return sb.ToString();
        }

        public StoredProcedureElement() : base(ElementType.PROC)
        {
        }
    }
}