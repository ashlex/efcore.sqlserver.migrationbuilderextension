namespace EFCore.SqlServer.MigrationBuilderExtensions.StoredProcedure
{
    public class InputParameterBuilder
    {
        private readonly InputParameter _parameter;

        internal InputParameterBuilder(InputParameter parameter)
        {
            _parameter = parameter;
        }

        public InputParameterBuilder Name(string name)
        {
            _parameter.Name = name;
            return this;
        }
        public InputParameterBuilder Type(string type)
        {
            _parameter.Type = type;
            return this;
        }
        
        public InputParameterBuilder DefaultValue(string defaultValue)
        {
            _parameter.DefaultValue = defaultValue;
            return this;
        }
        public InputParameterBuilder DefaultValue(int defaultValue)
        {
            _parameter.DefaultValue = defaultValue.ToString();
            return this;
        }
        
        public InputParameterBuilder Order(int order)
        {
            _parameter.Order = order;
            return this;
        }
    }
}