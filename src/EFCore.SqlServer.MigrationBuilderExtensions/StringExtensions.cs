using System;
using System.Security.Cryptography;
using System.Text;

namespace EFCore.SqlServer.MigrationBuilderExtensions
{
    public static class StringExtensions
    {
        public static string ToSha512(this string str)
        {
            using (var sha512 = new SHA512Managed())
            {

                var bytes = Encoding.UTF8.GetBytes(str);
                var hashBytes = sha512.ComputeHash(bytes);

                return BitConverter.ToString(hashBytes).Replace("-", "");
            }
        }
    }
}