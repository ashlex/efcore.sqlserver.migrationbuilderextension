using System.Collections.Generic;
using System.Text;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Triggers
{
    public class TriggerElement:ElementBase
    {
        internal string TargetName { get; set; }
        internal string TargetScheme { get; set; }
        internal ICollection<TriggerAction> AfterActions { get; set; }
        
        
        private string FullTargetName
        {
            get => "[" + TargetScheme + "].[" + TargetName + "]";
        }
        private string Actions
        {
            get
            {
                StringBuilder sb=new StringBuilder();
                foreach (TriggerAction action in AfterActions)
                {
                    sb.Append(action).Append(", ");
                }

                return sb.ToString().TrimEnd(' ', ',');
            }
        }

        internal override string ToSQL()
        {
            if (BuilderAction == BuilderAction.DROP) 
                return $"DROP {CurrentElementType} {FullName}";
            
            StringBuilder sb=new StringBuilder(Description?.ToSql());
            sb.Append(BuilderAction).Append(" ").Append(CurrentElementType).Append(" ").AppendLine(FullName)
                .Append("ON ").AppendLine(FullTargetName)
                .Append("AFTER ").AppendLine(Actions)
                .AppendLine("AS")
                .AppendLine("BEGIN")
                .AppendLine(Body)
                .AppendLine("END");
            return sb.ToString();
        }

        public TriggerElement() : base(ElementType.TRIGGER)
        {
        }
    }
}