namespace EFCore.SqlServer.MigrationBuilderExtensions.Triggers
{
    public enum TriggerAction
    {
        INSERT,
        DELETE,
        UPDATE
    }
}