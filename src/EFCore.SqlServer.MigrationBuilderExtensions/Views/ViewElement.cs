using System.Text;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Views
{
    public class ViewElement:ElementBase
    {
        internal override string ToSQL()
        {
            if (BuilderAction == BuilderAction.DROP) 
                return $"DROP {CurrentElementType} {FullName}";
            
            StringBuilder sb=new StringBuilder(Description?.ToSql());
            sb.Append(BuilderAction).Append(" ").Append(CurrentElementType).Append(" ").AppendLine(FullName);
            sb.AppendLine("AS")
//                .AppendLine("BEGIN")
                .AppendLine(Body);
//                .AppendLine("END");
            return sb.ToString();
        }

        public ViewElement() : base(ElementType.VIEW)
        {
        }
    }
}