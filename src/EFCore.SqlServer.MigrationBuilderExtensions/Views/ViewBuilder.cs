using System;
using EFCore.SqlServer.MigrationBuilderExtensions.Common;
using SqlKata;
using SqlKata.Compilers;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Views
{
    public class ViewBuilder:ElementBuilder<ViewElement,ViewBuilder>
    {
        internal ViewBuilder(ViewElement view) : base(view)
        {
        }
        
        public ViewBuilder Body(Action<Query> queryAction)
        {
            Query q=new Query();
            queryAction(q);
            SqlServerCompiler compiler=new SqlServerCompiler();
            _target.Body= compiler.Compile(q).Sql;
            return this;
        }
    }
}