using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCore.SqlServer.MigrationBuilderExtensions
{
    public interface IEntityConfigurator
    {
    }
    
    public interface IEntityConfigurator<TEntity>:IEntityConfigurator where TEntity: class 
    {
        void Configure(EntityTypeBuilder<TEntity> entity);
    }
    
}