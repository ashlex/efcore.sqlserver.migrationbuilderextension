using System;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    public class ElementBuilder<T,T1> 
        where T : ElementBase 
        where T1 :ElementBuilder<T,T1>
    {
        protected T _target;

        internal ElementBuilder(T target)
        {
            _target = target;
        }
        
        public T1 Name(string name)
        {
            _target.Name = name;
            return (T1)this;
        }
        
        public T1 Scheme(string scheme)
        {
            _target.Scheme = scheme;
            return (T1)this;
        }
        
        public T1 Body(string body)
        {
            _target.Body= body;
            return (T1)this;
        }

        public T1 AddDescription(Action<DescriptionBuilder> descriptionBuilder)
        {
            _target.Description=new Description();
            descriptionBuilder(new DescriptionBuilder(_target.Description));
            return (T1)this;
        }
    }

}