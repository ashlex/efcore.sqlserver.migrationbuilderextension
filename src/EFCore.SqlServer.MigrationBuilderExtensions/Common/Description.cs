using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    public class Description
    {
        private string _prefix = "--  ";
        
        private string _separator = new string('=', 50);
        private ICollection<Description> _log;

        internal string Summary { get; set; }
        internal string Author { get; set; }
        internal DateTime CreateDate { get; set; }

        internal ICollection<Description> Log => _log ?? (_log = new List<Description>());

        internal string ToSql()
        {
            StringBuilder sb=new StringBuilder();
            sb.Append(_prefix).AppendLine(_separator)
                .Append(_prefix).Append("Author:       ").AppendLine(Author)
                .Append(_prefix).Append("Create date:  ").AppendLine(CreateDate.ToString("yyyy-MM-dd"))
                .Append(_prefix).Append("Description:  ").AppendLine(Summary?.Replace(Environment.NewLine,Environment.NewLine+"--"));
            if (Log.Count > 0)
            {
                sb.Append(_prefix).AppendLine("ChangeLog:");
                sb.Append(_prefix).AppendLine($"{"Date",-15}{"Author",-15}Description");
                foreach (Description description in Log)
                {
                    sb.Append(_prefix).AppendLine(description.AsLogString());
                }
            }
                sb.Append(_prefix).AppendLine(_separator);
            return sb.ToString();
        }

        private string AsLogString()
        {
            return
                $"{CreateDate.ToString("yyyy-MM-dd"),-15}{(Author.Length > 14 ? Author.Substring(0, 14) : Author),-15}{Summary}";

        }
         
    }
}