namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    public abstract class ElementBase
    {

        public ElementBase(ElementType type)
        {
            CurrentElementType = type;
        }
        /// <summary>
        /// Description of trigger that will be placed at the beginning
        /// </summary>
        internal Description Description { get; set; }
        internal string Name { get; set; }
        internal string Scheme { get; set; }
        internal ElementType CurrentElementType { get; }
        internal string Body { get; set; }
        
        internal BuilderAction BuilderAction { get; set; }
        
        protected string FullName
        {
            get => "[" + Scheme + "].[" + Name + "]";
        }
        
        internal abstract string ToSQL();
    }

}