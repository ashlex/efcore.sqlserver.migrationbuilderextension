namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    public enum ElementType
    {
        TRIGGER,
        VIEW,
        PROC
    }
}