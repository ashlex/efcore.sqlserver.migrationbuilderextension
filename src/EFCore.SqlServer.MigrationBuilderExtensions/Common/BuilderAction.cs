namespace EFCore.SqlServer.MigrationBuilderExtensions.Common
{
    public enum BuilderAction
    {
        CREATE,
        ALTER,
        DROP
    }
    
}