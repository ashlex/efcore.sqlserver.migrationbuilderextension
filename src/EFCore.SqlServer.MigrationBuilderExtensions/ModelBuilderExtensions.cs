using Microsoft.EntityFrameworkCore;

namespace EFCore.SqlServer.MigrationBuilderExtensions
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder ApplySchemeConfiguration<T>(this ModelBuilder builder) where T:IModelConfigurator, new()
        {
        T conf=new T();
            conf.Configure(builder);
            return builder;
        }

        /// <summary>
        /// Configure single entity
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <typeparam name="TEntityConfigurator"></typeparam>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public static ModelBuilder ApplyEntityConfiguration<TEntityConfigurator, TEntity>(this ModelBuilder modelBuilder)
            where TEntityConfigurator : IEntityTypeConfiguration<TEntity>, new() where TEntity : class
        {
            var entityConfigurator = new TEntityConfigurator();
            modelBuilder.Entity<TEntity>(entityConfigurator.Configure);
            return modelBuilder;
        }
      
        
       
    } 
}