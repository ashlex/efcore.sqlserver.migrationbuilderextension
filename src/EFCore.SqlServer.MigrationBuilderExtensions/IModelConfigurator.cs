using Microsoft.EntityFrameworkCore;

namespace EFCore.SqlServer.MigrationBuilderExtensions
{
    public interface IModelConfigurator
    {
        void Configure(ModelBuilder modelBuilder);
    }
}